# Alcoholic Template for generic Web Development#

Alcohol is a basic setup to get you going quickly in almost any scenario, with the intention to include good design patterns: concating, minifying, uglifying code is set to the basic workfolw. 

### Tools and Modules in Alcohol ###

* Grunt
* npm
* Bower
* SASS

### Exact module list
npm:
   

    "grunt": "^0.4.5",
    "grunt-autoprefixer": "^3.0.0",
    "grunt-cli": "^0.1.13",
    "grunt-concurrent": "^1.0.0",
    "grunt-connect": "^0.2.0",
    "grunt-contrib-concat": "^0.5.1",
    "grunt-contrib-jshint": "^0.11.2",
    "grunt-contrib-uglify": "^0.9.1",
    "grunt-contrib-watch": "^0.6.1",
    "grunt-notify": "^0.4.1",
    "grunt-open": "^0.2.3",
    "grunt-postcss": "^0.4.0",
    "grunt-sass": "^1.0.0",
    "grunt-wiredep": "^2.0.0",
    "jquery": "^2.1.4",
    "load-grunt-tasks": "^3.1.0",
    "node-sass": "^3.1.1",
    "wiredep": "^2.2.2"

bower:

    "modular-main-normalize-scss": "~1.0.0",
    "jquery": "~2.1.4"


### Get started 

* Clone the repo
* Run these commmands in repo's root `npm install && bower install && grunt build && grunt serve`
* Start installing bower components with `bower install <component-name> --save && grunt wiredep`