//GRUNT Tips: http://24ways.org/2013/grunt-is-not-weird-and-hard/
//About grunt-sass module: https://github.com/sindresorhus/grunt-sass

module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            dist: {
                files: {
                    'client/build/main.min.css': 'client/build/main.min.css'
                }
            }
        },

        concat: {
            top: {
                src: [
                    //bower:js
                    'bower_components/jquery/dist/jquery.js',
                    //endbower
                ],
                dest: 'client/build/development-top.js',
            },

            bottom: {
                src: [
                'client/js/*.js'
                ],
                dest: 'client/build/development-bottom.js'
            },

        },

        uglify: {
            productionTop: {
                src: 'client/build/development-top.js',
                dest: 'client/build/production-top.min.js'
            },
            productionBottom: {
                src: 'client/build/development-bottom.js',
                dest: 'client/build/production-bottom.min.js'
            }
        },


        sass: {
            options: {
                //takes values such as 'nested' and 'compressed' Try 'em out!
                outputStyle: 'compressed',
            },
            dist: {

                files: {
                    'client/build/main.min.css': 'client/styles/main.scss',
                }
            }
        },

        connect: {
            server: {
                options: {
                    keepalive: true,
                    livereload: true
                }
            }
        },

        wiredep: {

            task: {

                // Point to the files that should be updated when
                // you run `grunt wiredep`
                src: [
                  'client/styles/main.scss',
                  'Gruntfile.js'
                ],
                options: {
                    // See wiredep's configuration documentation for the options
                    // you may pass:
                }
            }
        },
        open: {
            all: {
                // Gets the port from the connect configuration
                path: 'http://localhost:1337'
            }
        },
        concurrent: {
            serve: ['watch', 'connect'],
            sassconc: ['sass', 'jshint','concat'],
            prefixugly: ['autoprefixer', 'uglify'],
            options: {
                logConcurrentOutput: true
            }
        },

        jshint: {
            all: ["Gruntfile.js","client/js/**/*.js"],
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                globals: {
                    jQuery: true
                },
            },
 

        },

        watch: {
            options: {
            },
            css: {
                files: ['**/*.scss'],
                tasks: ['autoprefixer', 'sass'],
                options: {
                    spawn: false,
                    livereload:true
                }
            },

            concat: {
                files: ['**/*.js'],
                tasks: ['jshint','concat', 'uglify'],
                options: {
                    spawn: false,
                }
            },

            html: {
                files: ['*.html'],
                options: {
                    spawn: false,
                }
            }
        }


    });

    require('load-grunt-tasks')(grunt);

    grunt.registerTask('default', ['wiredep', 'concurrent:sassconc', 'concurrent:prefixugly','open', 'concurrent:serve']);
    grunt.registerTask('build', ['wiredep', 'concurrent:sassconc', 'concurrent:prefixugly']);
    grunt.registerTask('serve', ['open', 'concurrent:serve']);
};
